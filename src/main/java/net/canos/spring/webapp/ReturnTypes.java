package net.canos.spring.webapp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/return-types")
public class ReturnTypes {

	
	@RequestMapping(value = "/model", method = RequestMethod.GET)
	public String model(Model model){
		model.addAttribute("Hola","Mundo Model");
		
		Person p = new Person();
		p.setName("David");
		model.addAttribute("person", p);
		return "welcome";
	}
	
	@RequestMapping(value = "/modelMap", method = RequestMethod.GET)
	public String modelMap(ModelMap model){
		model.addAttribute("Hola","Mundo ModelMap");

		Person p = new Person();
		p.setName("Canós");
		model.addAttribute("person", p);
		return "welcome";
	}
	
	/*
	 * Ejemplo REST a pelo
	 */
	@RequestMapping(value = "/json/{name}/{surname}", method = RequestMethod.GET)
	@ResponseBody
	public Person getPerson2(@PathVariable String name) {
		Person p = new Person();
		p.setName(name);
		return p;
	}
	
	@RequestMapping(value = "/responseEntity", method = RequestMethod.GET)
	public ResponseEntity<Person> responseEntity(ModelMap model){		
		Person p = new Person();
		p.setName("Canós");
		MultiValueMap<String, String> headers = null;
		ResponseEntity<Person> response = new ResponseEntity<Person>(p,headers,HttpStatus.OK);
		return response;
	}
	
	
	
	
}
